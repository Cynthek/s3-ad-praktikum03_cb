package binaerbaum;

import org.eclipse.jdt.annotation.NonNull;

public class LinkedBinaryTree<E> implements BinaryTree<E>
{

    private NodeGenerisch<E> _root;

    /**
     * Dieser Konstruktor initialisiert einen neuen Baum, so dass er nur aus
     * einem Wurzelknoten mit dem gegebenen Wert besteht.
     * 
     * @param element
     *            Der Wert des Knotens.
     * @require element!=null
     */
    public LinkedBinaryTree(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_root = new NodeGenerisch<E>(element);
    }

    /**
     * Initialisert einen Baum mit einem Wurzelknoten, der den übergebenen
     * int-Wert hält und den an dem der übergebene linke Binärbaum hängt.
     * 
     * @param element
     *            Der Wert des neuen Wurzelknoten
     * @param leftTree
     *            Der linke Teilbaum
     * @require leftTree!=null
     */
    public LinkedBinaryTree(E element, @NonNull LinkedBinaryTree<E> leftTree)
    {
	_root = new NodeGenerisch<E>(element, leftTree.getRoot());
    }

    /**
     * Initialisert einen Baum mit einem Wurzelknoten, der den übergebenen
     * int-Wert hält und den an dem der übergebene linke und rechte Binärbaum
     * hängen.
     * 
     * @param element
     *            Der Wert des neuen Wurzelknoten
     * @param leftTree!=
     *            null, rightTree!=null
     */
    public LinkedBinaryTree(E element, LinkedBinaryTree<E> leftTree,
	    LinkedBinaryTree<E> rightTree)
    {
	_root = new NodeGenerisch<E>(element, leftTree.getRoot(),
		rightTree.getRoot());
    }

    /**
     * Liefert den Wurzelknoten.
     * 
     * @return Der Wurzelknoten Node
     */
    public NodeGenerisch<E> getRoot()
    {

	return _root;
    }

    /**
     * Liefert den im Wurzelknoten gehaltenen Wert.
     */
    public E getValue()
    {

	return _root.getValue();
    }

    /**
     * Liefert den gesamten linken Teilbaum.
     * 
     * @require _root.getLeftChild()!=null
     */
    public LinkedBinaryTree<E> getLeftTree()
    {
	assert _root
		.getLeftChild() != null : "Vorbedingung verletzt: _root.getLeftChild() != null";

	LinkedBinaryTree<E> leftTree = new LinkedBinaryTree<E>(
		_root.getLeftChild().getValue());
	leftTree._root = _root.getLeftChild();
	return leftTree;
    }

    /**
     * Liefert den gesamten rechten Teilbaum.
     * 
     * @require _root.getRightChild()!=null
     */
    public LinkedBinaryTree<E> getRightTree()
    {
	assert _root
		.getRightChild() != null : "Vorbedingung verletzt: _root.getRightChild() != null";
	LinkedBinaryTree<E> rightTree = new LinkedBinaryTree<E>(
		_root.getRightChild().getValue());
	rightTree._root = _root.getRightChild();
	return rightTree;
    }

    /**
     * Liefert die Anzahl aller im Baum enthaltenen Knoten.
     */
    public int getSize()
    {
	return _root.getNodeCount();
    }

    /**
     * Gibt den gesamten Baum auf der Konsole aus.
     */
    public String print()
    {
	return _root.printRecursive();
    }

    /**
     * Prüft, ob das übergebende Element in der Baumstruktur enthalten ist.
     * 
     *
     * @param element
     *            Das zu suchende Element
     * @require element!= null
     */
    public boolean contains(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	return _root.containsRecursive(element);
    }

    /**
     * Gibt die Anzahl von Knotenzugriffen zurück, die nach der Ausführung von
     * contains() durchlaufen wurden.
     *
     */
    public int getZaehler()
    {
	return _root.getZaehler();
    }
}
