package binaerbaum.startup;

import binaerbaum.LinkedBinaryTree;
import comparator.Person;

public class startup
{

    public static void main(String[] args)
    {
	LinkedBinaryTree<Integer> testTree = testTree();
	System.out.println(testTree.getSize());
	System.out.println(testTree.print());
	System.out.println(testTree.contains(6));
	System.out.println(testTree.getRoot().getZaehler());
    }

    private static LinkedBinaryTree<Integer> testTree()
    {

	LinkedBinaryTree<Integer> p1 = new LinkedBinaryTree<>(1);
	LinkedBinaryTree<Integer> p2 = new LinkedBinaryTree<>(2);
	LinkedBinaryTree<Integer> p3 = new LinkedBinaryTree<>(3);
	LinkedBinaryTree<Integer> p4 = new LinkedBinaryTree<>(4);

	LinkedBinaryTree<Integer> p5 = new LinkedBinaryTree<>(5, p1, p2);
	LinkedBinaryTree<Integer> p6 = new LinkedBinaryTree<>(6, p3, p4);

	LinkedBinaryTree<Integer> p7 = new LinkedBinaryTree<>(7, p5, p6);
	return p7;

    }

}
