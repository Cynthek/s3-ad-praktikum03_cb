package binaerbaum;

import static org.junit.Assert.*;

import org.junit.Test;
import comparator.Person;

public class LinkedBinaryTreePersonTest
{

    @Test
    public void testKonstruktorLinkedBinaryTreeInt()
    {
	Person karl = new Person("Karl", 12);
	LinkedBinaryTree<Person> testTreePerson = new LinkedBinaryTree<>(karl);
	assertEquals(karl, testTreePerson.getValue());

    }

    @Test
    public void testGetValue()
    {
	Person chris = new Person("Chris", 23);
	LinkedBinaryTree<Person> linkerTree = new LinkedBinaryTree<>(chris);
	assertEquals(chris, linkerTree.getValue());

	Person julia = new Person("Julia", 22);
	LinkedBinaryTree<Person> rechterTree = new LinkedBinaryTree<>(
		new Person("Peter", 30));
	LinkedBinaryTree<Person> großerTestTree = new LinkedBinaryTree<>(julia,
		linkerTree, rechterTree);
	assertEquals(julia, großerTestTree.getValue());

    }

    @Test
    public void testGetSize()
    {
	LinkedBinaryTree<Person> treelinks = new LinkedBinaryTree<>(
		new Person("Alex", 32));
	assertEquals(1, treelinks.getSize());

	LinkedBinaryTree<Person> zusammengesetzt = new LinkedBinaryTree<>(
		new Person("Bettina", 54), treelinks);
	assertEquals(2, zusammengesetzt.getSize());

	LinkedBinaryTree<Person> rechterTree = new LinkedBinaryTree<>(
		new Person("Gustav", 99));
	LinkedBinaryTree<Person> großerTree = new LinkedBinaryTree<>(
		new Person("Donald", 63), zusammengesetzt, rechterTree);
	assertEquals(4, großerTree.getSize());
    }

}
