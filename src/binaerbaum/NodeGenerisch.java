package binaerbaum;

/**
 * Exemplare dieser Klasse repräsentieren einen Knoten, der einen Wert und zwei
 * Kindknoten halten kann.
 * 
 * @author Julia, Chris
 */
public class NodeGenerisch<E>
{
    private E _object;
    private NodeGenerisch<E> _leftChild;
    private NodeGenerisch<E> _rightChild;
    private int _zaehler;
    /**
     * Initialisiert einen Knoten, der einen Wert hält.
     * @param object Der Wert des Knotens
     * @require element!=null
     */
    public NodeGenerisch(E element)
    {
	_object = element;
    }
    /**
     * Initialisiert einen Knoten mit einem Wert und einem linken Kindknoten.
     * @param element der Wert des Knotens
     * @param leftChild der linke Kindknoten
     * @require element!=null
     */
    public NodeGenerisch(E element, NodeGenerisch<E> leftChild)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_object = element;
	_leftChild = leftChild;
    }
    /**
     * Initialisiert einen Knoten mit einem Wert sowie linken und rechtem Kindknoten.
     * @param element der Wert des Knotens
     * @param leftChild das linke Kind
     * @param rightChild das rechte Kind
     * @require element!=null
     */
    public NodeGenerisch(E element, NodeGenerisch<E> leftChild,
	    NodeGenerisch<E> rightChild)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_object = element;
	_leftChild = leftChild;
	_rightChild = rightChild;
    }

    /**
     * Liefert den Wert der aktuellen Node zurueck.
     *
     * @return der vom Knoten gehaltene Wert.
     */
    public E getValue()
    {
	return _object;
    }

    /**
     * Gibt die Anzahl aller Kindknoten inkl. sich selbst zurück.
     * 
     * @return Anzahl aller Kindknoten
     */
    public int getNodeCount()
    {
	int result = 1;
	if (_leftChild != null)
	{
	    result = result + _leftChild.getNodeCount();
	}
	if (_rightChild != null)
	{
	    result = result + _rightChild.getNodeCount();
	}
	return result;
    }

    /**
     * Liefert den linken Kindknoten.
     * 
     * @return der linke Kindknoten
     */
    public NodeGenerisch<E> getLeftChild()
    {
	return _leftChild;
    }

    /**
     * Liefert den rechten Kindknoten.
     * 
     * @return der rechte Kindknoten
     */
    public NodeGenerisch<E> getRightChild()
    {
	return _rightChild;
    }

    /**
     * Gibt eine String-Repraesentation des BinaryTrees zurueck.
     */
    public String printRecursive()
    {

	String result = getValue() + " | ";
	if (_leftChild != null)
	{
	    result = result + _leftChild.printRecursive();
	    if (_rightChild != null)
	    {
		result = result + _rightChild.printRecursive();
	    }
	}
	return result;

    }

    /**
     * Sucht rekursiv den gesamten Baum nach
     * E element durch. Bei jedem rekursiven Aufruf wird der Zaehler
     * aktualisiert und setzt beim obersten Node anschließend die Anzahl
     * Node-Aufrufe als Summe.
     *
     * @param element das gesuchte Element
     * @return true, wenn Element enthalten ist, false, wenn nicht.
     */
    public boolean containsRecursive(E element)
    {
	_zaehler = 0;
	_zaehler++;
	if (this.getValue().equals(element))
	{
	    return true;
	}

	boolean istEnthalten = false;
	if (_leftChild != null)
	{
	    istEnthalten = _leftChild.containsRecursive(element);
	    _zaehler += _leftChild.getZaehler();
	}
	if (istEnthalten == false && _rightChild != null)
	{
	    istEnthalten = _rightChild.containsRecursive(element);
	    _zaehler += _rightChild.getZaehler();
	}

	return istEnthalten;
    }

    /**
     * Gibt zurück, wie oft containsRecursive das letzte Mal aufgerufen wurde.
     */
    public int getZaehler()
    {
	return _zaehler;
    }
}
