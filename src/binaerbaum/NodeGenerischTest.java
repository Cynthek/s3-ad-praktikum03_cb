/**
 * 
 */
package binaerbaum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import binaerbaum.NodeGenerisch;
import comparator.Person;

public class NodeGenerischTest
{

    @Test
    public void testgetValue()
    {
	NodeGenerisch<String> testNode = new NodeGenerisch<>("eins");
	assertEquals("eins", testNode.getValue());
	NodeGenerisch<Person> testNode2 = new NodeGenerisch<>(
		new Person("Otto", 45));
	assertEquals(new Person("Otto", 45), testNode2.getValue());
    }

    @Test
    public void testGetNodeCount()
    {
	NodeGenerisch<String> testNode1 = new NodeGenerisch<>("eins");
	NodeGenerisch<String> testNode2 = new NodeGenerisch<>("zwei");
	NodeGenerisch<String> testNode3 = new NodeGenerisch<>("drei", testNode1,
		testNode2);
	assertEquals(3, testNode3.getNodeCount());
	assertEquals(1, testNode1.getNodeCount());
	NodeGenerisch<Person> a = new NodeGenerisch<Person>(new Person("a", 1));
	NodeGenerisch<Person> b = new NodeGenerisch<Person>(new Person("b", 2));
	NodeGenerisch<Person> c = new NodeGenerisch<Person>(new Person("c", 3),a,b);
	assertEquals(1, a.getNodeCount());
	assertEquals(3, c.getNodeCount());

    }

    @Test
    public void testGetValue()
    {
	NodeGenerisch<Person> a = new NodeGenerisch<>(new Person("a", 1));
	assertEquals(new Person("a", 1), a.getValue());
	NodeGenerisch<String> b = new NodeGenerisch<String>("eins");
	assertEquals("eins", b.getValue());
    }

    @Test
    public void testGetLeftChild()
    {
	NodeGenerisch<String> testNode1 = new NodeGenerisch<>("eins");
	NodeGenerisch<String> testNode2 = new NodeGenerisch<>("zwei");
	NodeGenerisch<String> testNode3 = new NodeGenerisch<>("drei", testNode1,
		testNode2);
	assertEquals(testNode1, testNode3.getLeftChild());

	NodeGenerisch<Person> a = new NodeGenerisch<Person>(new Person("a", 1));
	NodeGenerisch<Person> b = new NodeGenerisch<Person>(new Person("b", 2));
	NodeGenerisch<Person> c = new NodeGenerisch<Person>(new Person("c", 3),a,b);
	assertEquals(a, c.getLeftChild());

    }

    @Test
    public void testGetRightChild()
    {
	NodeGenerisch<String> testNode1 = new NodeGenerisch<>("eins");
	NodeGenerisch<String> testNode2 = new NodeGenerisch<>("zwei");
	NodeGenerisch<String> testNode3 = new NodeGenerisch<>("drei", testNode1,
		testNode2);
	assertEquals(testNode2, testNode3.getRightChild());

	NodeGenerisch<Person> a = new NodeGenerisch<Person>(new Person("a", 1));
	NodeGenerisch<Person> b = new NodeGenerisch<Person>(new Person("b", 2));
	NodeGenerisch<Person> c = new NodeGenerisch<Person>(new Person("c", 3),a,b);
	assertEquals(b, c.getRightChild());

    }

    @Test
    public void testPrintRecursive()
    {
	NodeGenerisch<String> a = new NodeGenerisch<>("Eins");
	NodeGenerisch<String> b= new NodeGenerisch<>("Zwei");
	NodeGenerisch<String> c = new NodeGenerisch<String>("Drei",a,b);
	
	assertEquals("Eins | ", a.printRecursive());
	assertEquals("Zwei | ", b.printRecursive());
	assertEquals("Drei | Eins | Zwei | ", c.printRecursive());
    }

    @Test
    public void testContainsRecursive()
    {

	BinaryTree<String> testTree = erstelleStringTestbaum();

	assertEquals(true,testTree.contains("eins"));;
	assertEquals(true,testTree.contains("fünf"));
	assertEquals(true,testTree.contains("sieben"));
	assertEquals(false,testTree.contains("acht"));
	
	BinaryTree<Person> testTree2= erstellePersonTestbaum();
	assertEquals(true,testTree2.contains(new Person("a",1)));;
	assertEquals(true,testTree2.contains(new Person("e",5)));
	assertEquals(true,testTree2.contains(new Person("g",7)));
	assertEquals(false,testTree2.contains(new Person("Otto",44)));

	
    }

    private BinaryTree<Person> erstellePersonTestbaum()
    {
	LinkedBinaryTree<Person> p1 = new LinkedBinaryTree<>(new Person("a",1));
	LinkedBinaryTree<Person> p2 = new LinkedBinaryTree<>(new Person("b",2));
	LinkedBinaryTree<Person> p3 = new LinkedBinaryTree<>(new Person("c",3));
	LinkedBinaryTree<Person> p4 = new LinkedBinaryTree<>(new Person("d",4));

	LinkedBinaryTree<Person> p5 = new LinkedBinaryTree<>(new Person("e",5), p1, p2);
	LinkedBinaryTree<Person> p6 = new LinkedBinaryTree<>(new Person("f",6), p3, p4);

	LinkedBinaryTree<Person> p7 = new LinkedBinaryTree<>(new Person("g",7), p5, p6);

	return p7;
    }

    @Test
    public void testGetZaehler()
    {
	BinaryTree<String> testTree = erstelleStringTestbaum();

	testTree.contains("zwei");
	assertEquals(4, testTree.getZaehler());
	testTree.contains("sechs");
	assertEquals(5, testTree.getZaehler());
	testTree.contains("sieben");
	assertEquals(1, testTree.getZaehler());
	testTree.contains("vier");
	assertEquals(7, testTree.getZaehler());
    }

    private LinkedBinaryTree<String> erstelleStringTestbaum()
    {
	LinkedBinaryTree<String> p1 = new LinkedBinaryTree<>("eins");
	LinkedBinaryTree<String> p2 = new LinkedBinaryTree<>("zwei");
	LinkedBinaryTree<String> p3 = new LinkedBinaryTree<>("drei");
	LinkedBinaryTree<String> p4 = new LinkedBinaryTree<>("vier");

	LinkedBinaryTree<String> p5 = new LinkedBinaryTree<String>("fünf", p1, p2);
	LinkedBinaryTree<String> p6 = new LinkedBinaryTree<>("sechs", p3, p4);

	LinkedBinaryTree<String> p7 = new LinkedBinaryTree<>("sieben", p5, p6);

	return p7;
    }

}
