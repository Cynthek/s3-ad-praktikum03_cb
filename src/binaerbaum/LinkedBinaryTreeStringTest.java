package binaerbaum;

import static org.junit.Assert.*;

import org.junit.Test;

import comparator.Person;

public class LinkedBinaryTreeStringTest
{

    @Test
    public void testKonstruktorLinkedBinaryTreeInt()
    {
	LinkedBinaryTree<String> testTree = new LinkedBinaryTree<>("Wurzel");
	assertEquals("Wurzel", testTree.getValue());

	Person karl = new Person("Karl", 12);
	LinkedBinaryTree<Person> testTreePerson = new LinkedBinaryTree<>(karl);
	assertEquals(karl, testTreePerson.getValue());

    }

    @Test
    public void testGetValue()
    {

	LinkedBinaryTree<String> linkerTree = new LinkedBinaryTree<>("links");
	assertEquals("links", linkerTree.getValue());

	LinkedBinaryTree<String> rechterTree = new LinkedBinaryTree<>("rechts");
	LinkedBinaryTree<String> großerTestTree = new LinkedBinaryTree<>("oben",
		linkerTree, rechterTree);
	assertEquals("oben", großerTestTree.getValue());

    }

    @Test
    public void testGetSize()
    {
	LinkedBinaryTree<String> linkerTree = new LinkedBinaryTree<String>("links");
	assertEquals(1, linkerTree.getSize());

	LinkedBinaryTree<String> zusammengesetzt = new LinkedBinaryTree<>("zusammen", linkerTree);
	assertEquals(2, zusammengesetzt.getSize());

	LinkedBinaryTree<String> rechterTree = new LinkedBinaryTree<>("rechts");
	LinkedBinaryTree<String> großerTree = new LinkedBinaryTree<>("großerTree", zusammengesetzt,
		rechterTree);
	assertEquals(4, großerTree.getSize());
    }
}

