package binaerbaum;

public interface BinaryTree<E> {
	/**
	 * Gibt den im Wurzelknoten gehaltenen int-Wert zurück.
	 */
	public E getValue();

	/**
	 * Eine sondierende Operation, gibt den gesamten linken Teilbaum zurück.
	 */
	public BinaryTree<E> getLeftTree();

	/**
	 * Eine sondierende Operation, gibt den gesamten rechten Teilbaum zurück.
	 */
	public BinaryTree<E> getRightTree();

	/**
	 * Liefert den Wurzelknoten.
	 */
	public NodeGenerisch<E> getRoot();

	/**
	 * Gibt die Anzahl aller Knoten im Baum zurück.
	 */
	public int getSize();

	/**
	 * Gibt den gesamten Baum auf der Konsole aus.
	 */
	public String print();

	/**
	 * Prüft, ob das übergebende Element in der Baumstruktur enthalten ist.
	 *
	 * @param element Das zu suchende Element
	 */
	public boolean contains(E element);

	/**
	 * Gibt die Anzahl von Knotenzugriffen zurück, die nach der Ausführung von
	 * contains() durchlaufen wurden.

	 */
	public int getZaehler();

}
