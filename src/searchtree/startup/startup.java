package searchtree.startup;

import searchtree.BinaryTree;
import searchtree.SearchTree;

public class startup {

	public static void main(String[] args) {

		BinaryTree<Integer> testTree = erstelleTestbaum();

		System.out.println(testTree.print());
		System.out.println("Anzahl der Knoten: " + testTree.getSize());
		System.out.print(testTree.contains(8) + " ");
		System.out.println(testTree.getZaehler());
		System.out.print(testTree.contains(-5) + " ");
		System.out.println(testTree.getZaehler());

	}

	public static BinaryTree<Integer> erstelleTestbaum() {
		SearchTree<Integer> p1 = new SearchTree<>(5);
		p1.insert(4);
		p1.insert(7);
		p1.insert(2);
		p1.insert(10);
		p1.insert(11);
		p1.insert(8);
		p1.insert(13);

		return p1;
	}

}
