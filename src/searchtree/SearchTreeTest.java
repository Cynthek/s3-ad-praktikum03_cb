package searchtree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SearchTreeTest
{


    @Test
    public void testGetValue()
    {
	SearchTree<Integer> tree = new SearchTree<Integer>(1);
	assertEquals(1, (int)tree.getRoot().getValue());
    }

    @Test
    public void testGetSize()
    {
	BinaryTree<Integer> testTree = new SearchTree<Integer>(1);
	testTree.insert(2);
	testTree.insert(3);
	testTree.insert(4);
	testTree.insert(5);
	testTree.insert(6);
	testTree.insert(7);
	assertEquals(7,testTree.getSize());
    }
    
    @Test
    public void testInsertDoppelt()
    {
	BinaryTree<Integer> testTree = new SearchTree<Integer>(1);
	testTree.insert(2);
	testTree.insert(1);
	assertEquals(2,testTree.getSize());
    }

    @Test
    public void testInsertundContains()
    {
	BinaryTree<Integer> testTree = new SearchTree<Integer>(1);
	testTree.insert(2);
	assertEquals(true,testTree.contains(2)); 
	assertEquals(true, testTree.contains(1));
	assertEquals(false, testTree.contains(-3));
    }


    @Test
    public void testGetZaehler()
    {
	BinaryTree<Integer> testTree = new SearchTree<Integer>(1);
	testTree.insert(2);
	testTree.contains(1);
	assertEquals(1, testTree.getZaehler());
	testTree.contains(2);
	assertEquals(2, testTree.getZaehler());
	
	
    }


}
