package searchtree;

import org.eclipse.jdt.annotation.NonNull;

/**
 * 
 * @author Julia, Chris Diese Klasse repräsentiert einen Suchbaum.
 */
public class SearchTree<E extends Comparable<E>> implements BinaryTree<E>
{

    private NodeGeneric<E> _root;

    /**
     * Dieser Konstruktor initialisiert einen neuen Baum, so dass er nur aus
     * einem Wurzelknoten mit dem gegebenen Wert besteht.
     * 
     * @param element
     *            Der Wert des Knotens.
     * @require element!=null
     */
    public SearchTree(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_root = new NodeGeneric<E>(element);
    }

    /**
     * Liefert den Wurzelknoten.
     * 
     * @return Der Wurzelknoten Node
     */
    public NodeGeneric<E> getRoot()
    {

	return _root;
    }

    /**
     * Gibt die Anzahl von Knotenzugriffen zurück, die nach der Ausführung von
     * contains() durchlaufen wurden.
     */
    public int getZaehler()
    {
	return _root.getZaehler();
    }

    /**
     * Liefert den im Wurzelknoten gehaltenen Wert.
     */
    public E getValue()
    {

	return _root.getValue();
    }

    /**
     * Liefert den gesamten linken Teilbaum.
     * 
     * @require _root.getLeftChild()!=null
     */
    public SearchTree<E> getLeftTree()
    {
	assert _root
		.getLeftChild() != null : "Vorbedingung verletzt: _root.getLeftChild() != null";

	SearchTree<E> leftTree = new SearchTree<E>(
		_root.getLeftChild().getValue());
	leftTree._root = _root.getLeftChild();
	return leftTree;
    }

    /**
     * Liefert den gesamten rechten Teilbaum.
     * 
     * @require _root.getRightChild()!=null
     */
    public SearchTree<E> getRightTree()
    {
	assert _root
		.getRightChild() != null : "Vorbedingung verletzt: _root.getRightChild() != null";
	SearchTree<E> rightTree = new SearchTree<E>(
		_root.getRightChild().getValue());
	rightTree._root = _root.getRightChild();
	return rightTree;
    }

    /**
     * Liefert die Anzahl aller im Baum enthaltenen Knoten.
     */
    public int getSize()
    {
	return _root.getNodeCount();
    }

    /**
     * Fügt ein Objekt an der richtigen Stelle im Baum ein.
     * @param element
     * @require element!=null
     */
    
    public void insert(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_root.insertRecursive(element);

    }

    /**
     * Prüft, ob das übergebende Element in der Baumstruktur enthalten ist.
     *
     * @param element Das zu suchende Objekt.
     * @return true, wenn das Objekt gefunden wurde, false wenn nicht.
     */
    public boolean contains(E element)
    {
	return _root.containsRecursive(element);
    }

    /**
     * Gibt eine Stringbeschreibung des gesamten Baumes zurück.
     * @return Eine Stringrepräsentation des Baums.
     */
    public String print()
    {
	return _root.printRecursive();
    }
}
