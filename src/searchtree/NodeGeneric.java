package searchtree;

/**
 * 
 * @author Julia, Chris Diese Klasse repräsentiert Knoten, die einen Wert und
 *         Kindknoten halten.
 */
public class NodeGeneric<E extends Comparable<E>>
{
    private E _value;
    private NodeGeneric<E> _leftChild;
    private NodeGeneric<E> _rightChild;
    private int _zaehler;

    /**
     * Initialisiert einen Knoten ohne Kindknoten.
     * 
     * @param value
     *            Der Wert des Knotens.
     * @require element!=null
     */
    public NodeGeneric(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_value = element;
    }

    /**
     * Initialisiert einen Knoten mit einem linken Kindknoten.
     * 
     * @require element!=null
     */
    public NodeGeneric(E element, NodeGeneric<E> leftChild)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_value = element;
	_leftChild = leftChild;
    }

    /**
     * Initialisiert einen Knoten mit zwei Kindknoten.
     * 
     * @param value
     * @param leftChild
     * @param rightChild
     * @require element!=null
     */
    public NodeGeneric(E element, NodeGeneric<E> leftChild,
	    NodeGeneric<E> rightChild)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	_value = element;
	_leftChild = leftChild;
	_rightChild = rightChild;
    }

    /**
     * Liefert den linken Kindknoten.
     * 
     * @return der linke Kindknoten
     */
    public NodeGeneric<E> getLeftChild()
    {
	return _leftChild;
    }

    /**
     * Liefert den rechten Kindknoten.
     * 
     * @return der rechte Kindknoten
     */
    public NodeGeneric<E> getRightChild()
    {
	return _rightChild;
    }

    /**
     * Liefert den im Knoten gespeicherten Wert.
     */
    public E getValue()
    {
	return _value;
    }

    /**
     * Gibt die Anzahl aller Kindknoten inkl. sich selbst zurück.
     * 
     * @return Anzahl aller Kindknoten
     */
    public int getNodeCount()
    {
	int result = 1;
	if (_leftChild != null)
	{
	    result = result + _leftChild.getNodeCount();
	}
	if (_rightChild != null)
	{
	    result = result + _rightChild.getNodeCount();
	}
	return result;
    }

    public boolean hasLeftChild()
    {
	return _leftChild != null;
    }

    public boolean hasRightChild()
    {
	return _rightChild != null;
    }

    /**
     * Fügt ein Element an die richtige Stelle ein.
     * 
     * @param element
     *            Das einzufügende Element.
     * @require element!= null
     */
    public void insertRecursive(E element)
    {
	assert element != null : "Vorbedingung verletzt: element != null";
	int sort = element.compareTo(_value);
	if (sort == 0)
	{
	    return;
	}
	// Element ist kleiner als Wurzelknoten
	if (sort < 0 && this.hasLeftChild())
	{
	    this.getLeftChild().insertRecursive(element);
	}
	if (sort < 0 && !hasLeftChild())
	{
	    _leftChild = new NodeGeneric<E>(element);
	}
	// Element ist größer als Wurzelknoten
	if (sort > 0 && this.hasRightChild())
	{
	    this.getRightChild().insertRecursive(element);
	}
	if (sort > 0 && !hasRightChild())
	{
	    _rightChild = new NodeGeneric<E>(element);
	}
    }

    /**
     * Gibt den Baum als String im Preorder-Tiefendurchlauf zurück.
     * 
     * @return Eine String-Repräsentation des Baumes.
     */
    public String printRecursive()
    {

	String result = this.getValue().toString();
	if (getLeftChild() != null)
	{
	    result = result + "\nlinkes Kind: "
		    + getLeftChild().printRecursive();
	}
	if (getRightChild() != null)
	{
	    result = result + "\nrechtes Kind: "
		    + getRightChild().printRecursive();
	}
	return result;
    }

    public boolean containsRecursive(E element)
    {
	_zaehler=0;
	_zaehler++;
	int sort = element.compareTo(_value);
	boolean result = false;
	if (element.equals(this._value))
	{
	    result = true;
	}
	// Element ist kleiner als Wurzelknoten
	if (sort < 0 && this.hasLeftChild())
	{
	    result = this.getLeftChild().containsRecursive(element);
	    _zaehler += getLeftChild().getZaehler();
	}

	// Element ist größer als Wurzelknoten
	if (sort > 0 && this.hasRightChild())
	{
	    result = this.getRightChild().containsRecursive(element);
	    _zaehler += getRightChild().getZaehler();
	}

	return result;
    }
    /**
     * Gibt die Anzahl der letzen Aufrufe von containsRecursive() zurück.
     */
    public int getZaehler()
    {
	return _zaehler;
    }

}
