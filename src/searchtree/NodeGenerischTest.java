
package searchtree;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import comparator.Person;

public class NodeGenerischTest
{

    @Test
    public void testgetValue()
    {
	NodeGeneric<Integer> testNode = new NodeGeneric<>(1);
	assertEquals(1,(int)testNode.getValue());
    }
    @Test
    public void testGetLeftChild()
    {
	NodeGeneric<Integer> a= new NodeGeneric<Integer>(1);
	NodeGeneric<Integer> b = new NodeGeneric<Integer>(2);
	NodeGeneric<Integer>c= new NodeGeneric<Integer>(3, a, b);
	assertEquals(a, c.getLeftChild());
    }
    @Test
    public void testGetRightChild()
    {
	NodeGeneric<Integer> a= new NodeGeneric<Integer>(1);
	NodeGeneric<Integer> b = new NodeGeneric<Integer>(2);
	NodeGeneric<Integer>c= new NodeGeneric<Integer>(3, a, b);
	assertEquals(b, c.getRightChild());
    }
    @Test
    public void testInsertRecursive()
    {
	NodeGeneric<Integer> a = new NodeGeneric<Integer>(1);
	a.insertRecursive(3);
	assertEquals(3,(int) a.getRightChild().getValue());
	
	a.insertRecursive(-2);
	assertEquals(-2,  (int)a.getLeftChild().getValue());
	assertEquals(true, a.containsRecursive(-2));
	assertEquals(true, a.containsRecursive(1));
	a.insertRecursive(4);
	assertEquals(true, a.containsRecursive(4));
	a.insertRecursive(-10);
	assertEquals(true, a.containsRecursive(-10));
	a.insertRecursive(-10);
	
    }

    @Test
    public void testGetNodeCount()
    {
	NodeGeneric<String> testNode1 = new NodeGeneric<>("eins");
	assertEquals(1, testNode1.getNodeCount());
	NodeGeneric<String> c = new NodeGeneric<String>("c");
	NodeGeneric<String> x = new NodeGeneric<String>("x",testNode1,c);
	assertEquals(3, x.getNodeCount());
	assertEquals(1, c.getNodeCount());


    }

    @Test
    public void testGetValue()
    {
	NodeGeneric<Person> a = new NodeGeneric<>(new Person("a", 1));
	assertEquals(new Person("a", 1), a.getValue());
	NodeGeneric<String> b = new NodeGeneric<String>("eins");
	assertEquals("eins", b.getValue());
    }

    @Test
    public void testContainsRecursive()
    {

	NodeGeneric<Integer> a = new NodeGeneric<Integer>(2);
	NodeGeneric<Integer> b = new NodeGeneric<Integer>(4);
	NodeGeneric<Integer> c = new NodeGeneric<Integer>(3,a,b);
	assertEquals(true, c.containsRecursive(2));
	assertEquals(false, c.containsRecursive(10));
	c.insertRecursive(5);
	assertEquals(true, c.containsRecursive(5));
	
    }

    @Test
    public void testGetZaehler()
    {
	NodeGeneric<Integer> a = new NodeGeneric<Integer>(2);
	NodeGeneric<Integer> b = new NodeGeneric<Integer>(4);
	NodeGeneric<Integer> c = new NodeGeneric<Integer>(3,a,b);
	c.containsRecursive(3);
	assertEquals(1, c.getZaehler());
	c.containsRecursive(-4);
	assertEquals(2, c.getZaehler());
    }
    
    

}
