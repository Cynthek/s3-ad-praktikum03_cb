/**
 * 
 */
package comparator;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Christian
 * @version 15.11.2016
 *
 * @since 15.11.2016 , 18:59:27
 *
 */
public class BucketSortTest
{

    /**
     * Test method for {@link comparator.BucketSort#sort(comparator.Person[])}.
     */
    @Test
    public void testSort()
    {
	ListenWerkzeug listenwerkzeug = new ListenWerkzeug();
	Person[] personenArray = listenwerkzeug.erstelleArrayRandomAlter();

	personenArray = BucketSort.sort(personenArray);

	for (int i = 0; i < personenArray.length; i++)
	{
	    if (i > 0)
	    {
		assertEquals(true, personenArray[i]
			.getAlter() > personenArray[i - 1].getAlter());
	    }
	}
    }

    /**
     * Test method for {@link comparator.BucketSort#sort(comparator.Person[])}.
     */
    @Test
    public void testSortEinePerson()
    {
	ListenWerkzeug listenwerkzeug = new ListenWerkzeug();
	Person[] personenArray =
	{ new Person("Hans", 10) };

	personenArray = BucketSort.sort(personenArray);

	for (int i = 0; i < personenArray.length; i++)
	{
	    if (i > 0)
	    {
		assertEquals(true, personenArray[i]
			.getAlter() > personenArray[i - 1].getAlter());
	    }
	}
    }

}
