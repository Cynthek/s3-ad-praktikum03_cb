/**
 * 
 */
package comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Christian
 * @version 08.11.2016
 *
 * @since 08.11.2016 , 17:23:54
 *
 */
public class ListenWerkzeug {

	public List<Person> erstelleListe() {
		List<Person> result = new ArrayList<Person>();
		Random r = new Random();

		Person[] personen = new Person[10];
		personen[0] = new Person("Anna", 10);
		personen[1] = new Person("Bodo", 23);
		personen[2] = new Person("Bert", 23);
		personen[3] = new Person("Dennis", 42);
		personen[4] = new Person("Emma", 23);
		personen[5] = new Person("Fritz", 52);
		personen[6] = new Person("Gunther", 53);
		personen[7] = new Person("Helga", 52);
		personen[8] = new Person("Ingrid", 32);
		personen[9] = new Person("Julius", 15);

		while (result.size() != 9) {
			result.add(personen[r.nextInt(9)]);
		}

		return result;
	}

	public List<Person> erstelleListeRandomAlter() {
		List<Person> result = new ArrayList<Person>();
		Random r = new Random();

		Person[] personen = new Person[10];
		personen[0] = new Person("Anna", r.nextInt(100));
		personen[1] = new Person("Bodo", r.nextInt(100));
		personen[2] = new Person("Bert", r.nextInt(100));
		personen[3] = new Person("Dennis", r.nextInt(100));
		personen[4] = new Person("Emma", r.nextInt(100));
		personen[5] = new Person("Fritz", r.nextInt(100));
		personen[6] = new Person("Gunther", r.nextInt(100));
		personen[7] = new Person("Helga", r.nextInt(100));
		personen[8] = new Person("Ingrid", r.nextInt(100));
		personen[9] = new Person("Julius", r.nextInt(100));

		while (result.size() != 9) {
			result.add(personen[r.nextInt(9)]);
		}

		return result;
	}

	public Person[] erstelleArrayRandomAlter() {
		List<Person> result = new ArrayList<Person>();
		Random r = new Random();

		Person[] personen = new Person[10];
		personen[0] = new Person("Anna", r.nextInt(100));
		personen[1] = new Person("Bodo", r.nextInt(100));
		personen[2] = new Person("Bert", r.nextInt(100));
		personen[3] = new Person("Dennis", r.nextInt(100));
		personen[4] = new Person("Emma", r.nextInt(100));
		personen[5] = new Person("Fritz", r.nextInt(100));
		personen[6] = new Person("Gunther", r.nextInt(100));
		personen[7] = new Person("Helga", r.nextInt(100));
		personen[8] = new Person("Ingrid", r.nextInt(100));
		personen[9] = new Person("Julius", r.nextInt(100));

		while (result.size() != 9) {
			result.add(personen[r.nextInt(9)]);
		}

		// Erstelle Array
		Person[] personArr = new Person[result.size()];
		for (int i = 0; i < result.size(); i++) {
			personArr[i] = result.get(i);
		}

		return personArr;
	}

}
