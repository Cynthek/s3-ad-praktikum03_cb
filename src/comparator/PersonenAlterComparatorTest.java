/**
 * 
 */
package comparator;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 08.11.2016 / 20:52:16, Christian
 *
 * @author Christian
 * @author Julia
 */
public class PersonenAlterComparatorTest
{

    @Test
    public void testCompareTo()
    {
	Person a = new Person("Hans", 10);
	Person b = new Person("Ingrid", 10);
	PersonenAlterComparator comparator = new PersonenAlterComparator();

	assertEquals(0, comparator.compare(a, b));

	a = new Person("Hans", 9);
	b = new Person("Ingrid", 10);

	assertEquals(-1, comparator.compare(a, b));

	a = new Person("Hans", 12);
	b = new Person("Ingrid", 10);

	assertEquals(1, comparator.compare(a, b));
    }
    
    @Test(expected=NullPointerException.class)
    public void testExceptions()
    {
	Person a = new Person("Hans",10);
	a.compareTo(null);
    }

}
