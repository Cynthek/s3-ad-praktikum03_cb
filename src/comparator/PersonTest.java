/**
 * 
 */
package comparator;

import static org.junit.Assert.*;

import org.junit.Test;



/**
 * Testklasse für Personen.
 * @author Julia, Chris
 */
public class PersonTest
{

    @Test
    public void testEquals()
    {
	Person a = new Person("Hans", 20);
	Person b = new Person("Hans", 20);
	assertEquals(a, b);
	b = new Person("Hans", 25);
	assertFalse(a.equals(b));
	
    }

    @Test
    public void testCompareTo()
    {
	Person a = new Person("Hans", 20);
	Person b = new Person("Hans", 20);
	assertEquals(0, a.compareTo(b));
	b = new Person("Hans", 21);
	assertEquals(-1, a.compareTo(b));
	b = new Person("Hans", 19);
	assertEquals(1, a.compareTo(b));
	b = new Person("Ingrid", 19);
	assertEquals(-1, a.compareTo(b));
	b = new Person("Gerhard", 25);
	assertEquals(1, a.compareTo(b));
    }

    @Test
    public void testGetName()
    {
	Person karl = new Person("Karl", 12);
	assertEquals("Karl", karl.getName());
    }

    @Test
    public void testGetAlter()
    {
	Person karl = new Person("Karl", 12);
	assertEquals(12, karl.getAlter());
    }

    @Test
    public void testHashCode()
    {
	// Namen mit den gleichen Buchstaben geben unterschiedlichen Hashcode
	// aus
	Person lara = new Person("Lara", 24);
	Person aral = new Person("Aral", 24);
	assertEquals(false, lara.hashCode() == aral.hashCode());

	Person lara2 = new Person("Lara", 24);
	assertEquals(true, lara.hashCode() == lara2.hashCode());
    }

    @Test(expected=NullPointerException.class)
    public void testExceptions()
    {
	Person a = new Person("Hans",10);
	a.compareTo(null);
    }

}
