/**
 * 
 */
package comparator.startup;

import comparator.BucketSort;
import comparator.ListenWerkzeug;
import comparator.Person;

/**
 * 07.11.2016 / 23:01:00, Christian
 *
 * @author Christian
 * @author (co-author)
 * @version 07.11.2016
 *
 * @since 07.11.2016 , 23:01:00
 *
 */
public class Startup_Bucketsort {

	public static void main(String[] args) {
		ListenWerkzeug listenwerkzeug = new ListenWerkzeug();

		// Liste erstellen
		Person[] personenArray = listenwerkzeug.erstelleArrayRandomAlter();

		// Vor der Sortierung
		for (Person person : personenArray) {
			System.out.println("[" + person.getName() + "," + person.getAlter() + "],");
		}

		// Sortierung
		personenArray = BucketSort.sort(personenArray);
		System.out.println("SORTIERUNG WURDE DURCHGEFÜHRT");

		// Nach der Sortierung
		for (Person person : personenArray) {
			System.out.println("[" + person.getName() + "," + person.getAlter() + "],");
		}
	}

}
