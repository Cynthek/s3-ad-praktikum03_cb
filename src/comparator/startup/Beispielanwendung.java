package comparator.startup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import comparator.Person;
import comparator.PersonenAlterComparator;

/**
 * Diese Beispielanwendung erstellt eine unsortierte Liste von Personen, 
 * sortiert sie nach Name und nach Alter und gibt die jeweiligen Listen auf
 * der Konsole aus.
 * @author Julia-Marie, Chris
 *
 */
public class Beispielanwendung
{
    public static void main(String[] args)
    {
	ArrayList <Person> unsortiert = erstelleListe();
	druckeListe(unsortiert);
	
	System.out.println("\n");
	unsortiert.sort(null);
	druckeListe(unsortiert);
	
	PersonenAlterComparator vergleicher=new PersonenAlterComparator();
	unsortiert.sort(vergleicher);
	System.out.println("\n");
	druckeListe(unsortiert);
	
	//Liste von Strings sortieren (Uppercase vor Lowercase ignorieren)
	ArrayList liste = erstelleStringliste();
	System.out.println("\n");
	System.out.println(liste.toString());
	liste.sort(String.CASE_INSENSITIVE_ORDER);
	System.out.println(liste.toString());
	
	Comparator<String> comparator= Collections.reverseOrder(String.CASE_INSENSITIVE_ORDER);
	liste.sort(comparator);
	System.out.println(liste);

    }
    private static ArrayList<String> erstelleStringliste()
    {
	ArrayList<String> liste = new ArrayList<>();
	liste.add("aasgeier");
	liste.add("zebra");
	liste.add("Alphabet");
	liste.add("Xaver");
	liste.add("peter");
	return liste;
    }

    private static ArrayList<Person> erstelleListe()
    {
	ArrayList<Person> liste = new ArrayList<>();
	liste.add(new Person("Karl", 12));
	liste.add(new Person("Claudia", 34));
	liste.add(new Person("Xaver", 42));
	liste.add(new Person("Peter", 12));
	liste.add(new Person("Karl", 14));
	liste.add(new Person ("kristoff",23));
	liste.add(new Person("Anja", 21));
	liste.add(new Person("Waldtraud", 77));
	liste.add(new Person("Anke", 21));
	liste.add(new Person("Christian", 23));
	liste.add(new Person("Julia", 22));
	liste.add(new Person("Rüdiger", 64));
	
	
	return liste;
    }

    private static void druckeListe(ArrayList<Person> liste)
    {
	for (int i = 0; i < liste.size(); i++)
	{
	    System.out.println(liste.get(i).toString());
	}
    }

}