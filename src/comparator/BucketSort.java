/**
 * 
 */
package comparator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Christian
 * @version 08.11.2016
 *
 * @since 08.11.2016 , 22:13:24
 *
 */
public class BucketSort {

	/**
	 * Sortiert ein Array von Personen anhand ihres Alters aufsteigend.
	 * 15.11.2016 / 17:39:43, Christian
	 * 
	 * @require personArr != null
	 * @param personArr
	 * @return
	 */
	public static Person[] sort(Person[] personArr) {

		assert personArr != null : "Vorbedingung verletzt: personArr != null";

		// Wenn Array leer ist, abbrechen
		if (personArr.length == 0) {
			return null;
		}

		// Behaelter initialisieren
		Set<Person>[] beheaelter = new HashSet[150];

		for (int i = 0; i < beheaelter.length; i++) {
			beheaelter[i] = new HashSet<Person>();
		}

		// Personen in Behaelter sortieren
		for (Person person : personArr) {
			beheaelter[person.getAlter()].add(person);
		}

		// Array konstruieren
		List<Person> tempArr = new ArrayList<Person>();

		for (int i = 0; i < beheaelter.length; i++) {
			for (Person person : beheaelter[i]) {
				tempArr.add(person);
			}
		}

		Person[] result = new Person[tempArr.size()];

		for (int i = 0; i < tempArr.size(); i++) {
			result[i] = tempArr.get(i);
		}

		return result;

	}

}
