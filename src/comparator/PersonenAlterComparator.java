/**
 * 
 */
package comparator;

import java.util.Comparator;

/**
 * 08.11.2016 / 17:48:07, Christian
 *
 * @author Christian
 * @author Julia Dieser Comparator vergleicht zwei Personen anhand von Alter und
 *         Namen.
 */
public class PersonenAlterComparator implements Comparator<Person>
{

    /**
     * Vergleicht zwei Personen anhand ihres Alters.
     * 
     * @return 0 Das Alter ist gleich.
     * @return -1 Die zweite Person ist älter als die erste.
     * @return 1 Die erste Person ist älter als die zweite.
     */
    @Override
    public int compare(Person o1, Person o2) throws NullPointerException
    {
	if (o1 == null || o2 == null)
	{
	    throw new NullPointerException();
	}

	if (o1.getAlter() > o2.getAlter())
	{
	    return 1;
	}
	else if (o1.getAlter() < o2.getAlter())
	{
	    return -1;
	}
	else
	{
	    return 0;
	}
    }
}
