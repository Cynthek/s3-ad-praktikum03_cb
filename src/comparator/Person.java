/**
 * 
 */
package comparator;

/**
 * @author Christian, Julia
 * 
 *         Exemplare dieser Klasse repräsentieren eine Person mit einem Namen
 *         und einem Alter. Zwei Personen lassen sich anhand von Namen und Alter
 *         vergleichen.
 * 
 */
public class Person implements Comparable<Person>
{
    private String _name;
    private int _alter;

    /**
     * 
     * @param name
     *            Der Name der Person
     * @param alter
     *            Alter der Person
     * @require alter>=0
     */
    public Person(String name, int alter)
    {
	assert alter >= 0 : "Vorbedingung verletzt: alter>=0";
	_name = name;
	_alter = alter;
    }

    /**
     * Redefinierung der Equals Methode
     */
    @Override
    public boolean equals(Object o)
    {
	boolean result = false;

	if (o instanceof Person)
	{
	    Person og = (Person) o;
	    result = this.getAlter() == og.getAlter()
		    && this.getName().equals(((Person) o).getName());
	}
	return result;
    }

    /**
     * Gibt den Namen der Person zurück.
     * 
     * @return Name der Person
     */
    public String getName()
    {
	return _name;
    }

    /**
     * Gibt das Alter der Person zurück.
     * 
     * @return Alter der Person in Jahren
     */
    public int getAlter()
    {
	return _alter;
    }

    @Override
    public int hashCode()
    {
	int hashcode = 0;
	for (int i = 0; i < this.getName().length(); i++)
	{
	    hashcode = hashcode * Character.getNumericValue(getName().charAt(i))
		    + i;
	}
	return hashcode * getAlter();
    }

    @Override
    /**
     * Gibt eine Stringrepräsentation der Person aus.
     */
    public String toString()
    {
	return "Name: " + getName() + ", Alter: " + getAlter();
    }

    @Override
     /**
      * Diese Methode ordnet zwei Personen nach ihrem Namen, oder wenn dieser
      * gleich ist, nach ihrem Alter.
      * 
      * @param vergleichsperson
      *            Die Person, mit der verglichen werden soll.
      * @return 0, wenn Name und Alter gleich sind,
      * @return -1 , wenn der Name der zu vergleichenden Person später im
      *         Alphabet vorkommt,
      * @return 1, wenn der Name der zu vergleichenden Person früher im Alphabet
      *         kommt
      */
     public int compareTo(Person vergleichsperson) throws NullPointerException
     {
	if (vergleichsperson==null)
	{
	    throw new NullPointerException();
	}
 	
 	// Wenn Name und Alter gleich sind:
 	if (this.equals(vergleichsperson))
 	{
 	    return 0;
 	}
 	// Wenn Name gleich ist, nach Alter sortieren
 	if (this._name.equals(vergleichsperson._name))
 	{
 	    Integer alter1= this.getAlter();
 	    Integer alter2=vergleichsperson.getAlter();
 	    return alter1.compareTo(alter2);
 	}
 	else 
 	{
 	    return _name.toLowerCase().compareTo(vergleichsperson.getName().toLowerCase());
 	}
     }
}