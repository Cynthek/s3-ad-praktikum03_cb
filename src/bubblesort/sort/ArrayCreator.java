package bubblesort.sort;

import java.util.Random;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Hilfsklasse, um Arrays fuer Sortieralgorithmen zu erzeugen.
 * 
 * Hinweis: wenn in Eclipse die Fehlermeldung angezeigt wird: "the import
 * org.eclipse cannot be resolved" oder "NonNull cannot be resolved to a type"
 * Kann das gelöst werden durch diesen QuickFix: "copy library with default null
 * annotations to build path"
 * 
 * @author Petra Becker-Pechau
 * @version WiSe 2016
 */
public class ArrayCreator {

	/**
	 * Liefert ein Array mit aufsteigend sortieren Werten ohne Doppelte
	 */
	public static int @NonNull [] createAufsteigendSortiert(int size) {
		int[] result = new int[size];
		for (int i = 0; i < size; ++i) {
			result[i] = i;
		}
		return result;
	}

	/**
	 * Liefert ein Array mit aufsteigend sortieren Werten mit Doppelten
	 */
	public static int @NonNull [] createAufsteigendMitDoppelten(int size) {
		int[] result = new int[size];
		for (int i = 0; i < size; ++i) {
			result[i] = (int) (i * 0.7);
		}
		return result;
	}

	/**
	 * Liefert ein Array mit absteigend sortieren Werten ohne Doppelte
	 */
	public static int[] createAbsteigend(int size) {
		int[] result = new int[size];

		for (int i = 0; i < size; ++i) {
			result[i] = size - 1 - i;
		}
		return result;
	}

	/**
	 * Fuellt das Array so, dass die Eintraege tendenziell zwischen hohen und
	 * niedrigen Werten hin und her springen.
	 */
	public static int[] createAlternierend(int size) {
		int[] result = new int[size];

		for (int i = 0; i < size; i += 2) {
			result[i] = i;
		}
		for (int i = 1, k = (size | 1) - 2; i < size; i += 2, k -= 2) {
			result[i] = k;
		}
		return result;
	}

	/**
	 * Liefert ein Array mit zufaellig angeordneten Eintraegen. DAs Array sieht
	 * nach jedem Aufruf anders aus.
	 */
	public static int @NonNull [] createGemischt(int size) {
		int[] result = createAufsteigendSortiert(size);
		return durchmische(result);
	}

	public static int[] createGemischtMitDoppelten(int size) {
		int[] result = createAufsteigendMitDoppelten(size);
		return durchmische(result);
	}

	private static int @NonNull [] durchmische(int @NonNull [] result) {
		Random zufall = new Random();

		for (int i = result.length - 1; i > 0; --i) {
			int index1 = i;
			int index2 = zufall.nextInt(i);
			int tmp = result[index1];
			result[index1] = result[index2];
			result[index2] = tmp;
		}
		return result;
	}

}
