package bubblesort;

import bubblesort.sort.ArrayCreator;

/**
 * 
 * @author Julia,Chris 
 * Erstellt ein zufälligs Array von Zahlen und sortiert sie
 *         in aufsteigender Reihenfolge.
 *
 */
public class Beispielclient
{

    public static void main(String[] args)
    {
	druckeOhneVisuals();
	visualisiere();

    }

    private static void druckeOhneVisuals()
    {
	int[] beispiel = ArrayCreator.createGemischt(20);
	for (int i = 0; i < beispiel.length; i++)
	{
	    System.out.println(beispiel[i]);
	}
	System.out.println("\n");
	Sortierer.sort(beispiel);
	for (int i = 0; i < beispiel.length; i++)
	{
	    System.out.println(beispiel[i]);
	}
    }

    private static void visualisiere()
    {
	int[] beispiel = ArrayCreator.createGemischt(20);
	System.out.println("\n");
	Sortierer.sortVisual(beispiel);
    }

}
