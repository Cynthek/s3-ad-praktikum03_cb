package bubblesort.visualwrapper;
import org.eclipse.jdt.annotation.NonNull;

/**
 * Wrapper fuer ein Array, erlaubt in-Place-Sortierung mit Swap.
 * 
 * Kann in implementierenden Klassen verwendet werden, um Arrays zu
 * visualisieren.
 * 
 * @author Petra Becker-Pechau
 * @version WI-AD WiSe 2016
 */
public interface ArrayWrapper
{

	/**
	 * @require index >= 0
	 * @require index < getLength()
	 */
	int get(int index);

	/**
	 * 
	 * @return get (getLength()-1)
	 */
	int getLast();

	/**
	 * 
	 * @ensure result = getLength()-1
	 */
	int getLastIndex();

	/**
	 * 
	 * @ensure result > 0
	 */
	int getLength();

	/**
	 * Tauscht den Inhalt der Array-Zellen mit dem Index i1 und i2
	 * 
	 * @require i1 >= 0
	 * @require i2 >= 0
	 * @require i1 < getLength()
	 * @require i2 < getLength()
	 */
	void swap(int i1, int i2);

	/**
	 * setzt den gegebenen value in die Array-Zelle mit dem gegebenen Index.
	 * 
	 * @require index >= 0
	 * @require index < getLength()
	 */
	void set(int index, int value);

	/**
	 * @ensure result != null
	 */
	@NonNull String toString();

	/**
	 * Liefert eine Kopie des gesamten enthaltenen Arrays.
	 * 
	 * @ensure result != null
	 */
	int @NonNull [] getCopy();

	/**
	 * Diese Operation braucht im Praktikum nicht verwendet zu werden, sie kann
	 * jedoch nuetztlich sein, um beispielweise Quicksort uebersichtlicher zu
	 * implementieren.
	 * 
	 * Gibt einen neuen ArrayWrapper zurueck, der dasselbe Array enthaelt, aber
	 * nur den definierten Abschnitt zugreifbar macht. Sind startIndexRelativ
	 * und endIndexRelativ gleich, dann hat der neue Abschnitt die Laenge 1
	 * 
	 * Weil wir in-place Sortieren wollen (also nichts umkopieren), bietet diese
	 * Methode ein Alterative zum Kopieren in ein neues Array. Der neu erzeugte
	 * Wrapper verhaelt sich so, als enthielte er ein Array, das nur aus dem
	 * definierten Abschnitt besteht, in Wirklichkeit arbeitet er aber auf
	 * demselben Array.
	 * 
	 * Beispiel: Gegeben die Variable aw, die einen ArrayWrapper mit dem Array
	 * {10,11,12,13,14} enthaelt. Wenn man jetzt erst einmal nur den
	 * Teilabschnitt von Index 2 bis Index 4 sortieren moechte, kann man einen
	 * Wrapper fuer genau diesen Abschnitt erzeugen: ArrayWrapper
	 * abschnittsWrapper = wr.getSubAbschnitt(2,4). Dieses Exemplar steht fuer
	 * den Abschnitt {12,13,14}, mit get(0) beispielsweise erhaelt man die 12.
	 * Mit abschnittsWrapper.set(0, 17) wird die 12 ueberschrieben mit einer 17.
	 * Wichtig: der neue Wrapper arbeitet auf dem Original-Array.
	 * 
	 * @require abschnitt != null
	 * @require startIndex <= endIndexRelativ
	 * @require startIndexRelativ >= 0
	 * @require startIndexRelativ < getLength()
	 * @require endIndexRelativ >= 0
	 * @require endIndexRelativ < getLength()
	 */
	@NonNull ArrayWrapper getSubAbschnitt(int startIndexRelativ, int endIndexRelativ);

}