package bubblesort.visualwrapper;
import org.eclipse.jdt.annotation.NonNull;

/**
 * Dokumentation von ArrayWrappern allgemein und Dokumentation der Operationen:
 * siehe Interface ArrayWrapper.
 * 
 * Objekt dieser Klasse Visualiseren die Werte im Array als Punkte in einem
 * zweidimensionalen Koordinatensystem. Auf der x-Achse sind die Indizes, auf
 * der y-Achse der jeweilige int-Wert aufgetragen. Unten links ist die Position
 * 0/0.
 * 
 * Hinweis: wenn in Eclipse die Fehlermeldung angezeigt wird: "the import
 * org.eclipse cannot be resolved" oder "NonNull cannot be resolved to a type"
 * Kann das gelöst werden durch diesen QuickFix: "copy library with default null
 * annotations to build path"
 * 
 * @author Petra Becker-Pechau
 * @version WI-AD WiSe 2016
 */
public class VisualArrayWrapper extends ArrayWrapperImpl {
	private ArrayVisualizer _visualizer;

	/**
	 * Initialisiert einen neuen ArrayWrapper mit dem gegebenen Array (das Array
	 * wird nicht kopiert, sondern direkt als Referenzvariable verwendet)
	 * 
	 * @require array != null
	 * @require array.length > 0
	 */
	public VisualArrayWrapper(int @NonNull [] array) {
		super(array);

		_visualizer = new ArrayVisualizer(array);
	}

	/**
	 * Initialisiert einen neuen ArrayWrapper mit dem gegebenen Array (das Array
	 * wird nicht kopiert, sondern direkt als Referenzvariable verwendet)
	 * 
	 * @param verzoegerung
	 *            die Verzoegerung bei der Anzeige in ms
	 * 
	 * @require array != null
	 * @require array.length > 0
	 * @require verzoegerung >= 0
	 * 
	 */
	public VisualArrayWrapper(int @NonNull [] array, int verzoegerung) {
		super(array);
		_visualizer = new ArrayVisualizer(array, verzoegerung);
	}

	private VisualArrayWrapper(@NonNull VisualArrayWrapper visualArrayWrapper, int startIndexRelativ,
			int endIndexRelativ, ArrayVisualizer visualizer) {
		super(visualArrayWrapper, startIndexRelativ, endIndexRelativ);
		_visualizer = visualizer;
	}

	/**
	 * @require abschnitt != null
	 * @require startIndex < abschnitt.getLength()
	 * @require length >= 0
	 * @require length <= startIndex + abschnitt.getLength()
	 */
	public @NonNull VisualArrayWrapper getSubAbschnitt(int startIndexRelativ, int endIndexRelativ) {
		return new VisualArrayWrapper(this, startIndexRelativ, endIndexRelativ, _visualizer);
	}

	/**
	 * @require index >= 0
	 * @require index < getLength()
	 */
	public int get(int index) {
		_visualizer.hightlightPoint(_startIndex + index);
		return super.get(index);
	}

	/**
	 * @require i1 >= 0
	 * @require i2 >= 0
	 * @require i1 < getLength()
	 * @require i2 < getLength()
	 */
	public void swap(int i1, int i2) {
		assert i1 >= 0 : "i1 >= 0";
		assert i1 < getLength() : "i1 < getLength()";
		assert i2 >= 0 : "i2 >= 0";
		assert i2 < getLength() : "i2 < getLength()";

		_visualizer.hightlightPoint(i1);
		_visualizer.hightlightPoint(i2);

		super.swap(i1, i2);

		_visualizer.repaintPoints(_startIndex + i1, _startIndex + i2);

		_visualizer.hightlightPoint(i1);
		_visualizer.hightlightPoint(i2);

	}

	public void set(int index, int value) {
		super.set(index, value);
		_visualizer.hightlightPoint(index + _startIndex);
	}

}
