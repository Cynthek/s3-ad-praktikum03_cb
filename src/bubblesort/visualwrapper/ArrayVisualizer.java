package bubblesort.visualwrapper;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Diese Implementation des Interfaces InPlaceSortierbareIntListe stellt den
 * Zustand einer Liste von int-Werten grafisch dar. Auf der x-Achse ist die
 * Position in der Liste, auf der y-Achse der jeweilige int-Wert aufgetragen.
 * Unten links ist die Position 0/0.
 * 
 * 
 * @author Petra Becker-Pechau
 * @author Lars-Peter Clausen
 * @author Julian Fietkau
 * @author Fredrik Winkler
 * @author Axel Schmolitzky
 * @version WI-AD WiSe 2016
 */
class ArrayVisualizer {
	/**
	 * Enthaelt eine konstante Verzoegerung fuer die grafische Aktualisierung.
	 * Die Verzoegerung wird einmalig im Konstruktor abhaengig von der
	 * Listenlaenge berechnet.
	 */
	private final int _verzoegerung;

	/**
	 * Enthaelt das Anzeigepanel
	 */
	private final PunktePanel _panel;

	/**
	 * @require array != null
	 */
	public ArrayVisualizer(int[] array) {
		this(array, (int) (0.5 * Math.pow(7 - (Math.min(array.length, 108.0) / 18), 2)),
				Math.max(600 / array.length, 1));
	}

	/**
	 * @param verzoegerung
	 *            die Verzoegerung in ms
	 *
	 * @require array != null
	 * @require verzoegerung >= 0
	 * 
	 */
	public ArrayVisualizer(int[] array, int verzoegerung) {
		this(array, verzoegerung, Math.max(600 / array.length, 1));

	}

	/**
	 * @param verzoegerung
	 *            die Verzoegerung in ms
	 * @param punktGroesse
	 *            jeder int-Wert wird durch entsprechend viele Pixel dargestellt
	 *
	 * @require array != null
	 * @require verzoegerung >= 0
	 * @require punktGroesse > 0
	 */
	public ArrayVisualizer(int[] array, int verzoegerung, int punktGroesse) {
		_verzoegerung = verzoegerung;
		_panel = new PunktePanel(array, punktGroesse);
	}

	/**
	 * Aktualisiere die beiden Elemente in der Anzeige.
	 * 
	 * @require isValidIndex(i)
	 * @require isValidIndex(k)
	 */
	public void repaintPoints(int i, int k) {
		assert isValidIndex(i) : "isValidIndex(i)";
		assert isValidIndex(k) : "isValidIndex(k)";

		paint(i);
		paint(k);
	}

	public boolean isValidIndex(int i) {
		return i >= 0 && i < getLength();
	}

	public int getLength() {
		return _panel._intArray.length;
	}

	/**
	 * Gib den Wert an der angegebenen Position zurueck.
	 * 
	 * @param position
	 *            die Position des int-Werts im Array
	 * @throws IndexOutOfBoundsException
	 *             falls !existiert(position)
	 * @return der Wert an der angegebenen Position
	 */
	public void hightlightPoint(int position) {
		assert isValidIndex(position) : "isValidIndex(position)";

		_panel.selectPoint(position);
		paint(position);
		_panel.deselectPoint();
		_panel.zeichnePosition(position);
	}

	/**
	 * Zeichnet die gegebene Position neu und wartet kurz
	 */
	private void paint(int position) {
		_panel.zeichnePosition(position);
		try {
			Thread.sleep(_verzoegerung);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Dieses Panel dient zur Darstellung eines int-Arrays in einem
	 * Koordinatensystem.
	 * 
	 * Auf der X-Achse werden die Indizes aufgetragen, auf der Y-Achse die
	 * Werte. Es wird davon ausgegangen, dass das Array die Werte von 0 bis
	 * laenge-1 enthaelt. Fuer das Setzen eines Wertes wird eine zusaetzliche
	 * zeichne-Methode angeboten, um die Performanz zu erhoehen.
	 */
	private static class PunktePanel extends JPanel {
		private static final Color BACKGROUND_COLOR = Color.BLACK;
		private static final Color POINT_COLOR = Color.WHITE;
		private static final Color SELECTED_POINT_COLOR = Color.RED;
		private static final int RAND = 4;

		private final int[] _intArray;
		private final int _punktGroesse;
		private final int _ausdehnung;
		private int _selected;

		private final JFrame _frame;

		/**
		 * Erzeugt ein neues PunktePanel.
		 * 
		 * @param intArray
		 *            Die anzuzeigenden Werte.
		 * @param punktGroesse
		 *            Pixelanzahl, die zur Anzeige eines Punktes genutzt werden.
		 */
		private PunktePanel(int[] intArray, int punktGroesse) {
			_intArray = intArray;
			_punktGroesse = punktGroesse;
			_ausdehnung = intArray.length * punktGroesse;
			_selected = -1;

			// Initialisere dieses Panel
			setBackground(BACKGROUND_COLOR);
			int groesse = RAND + _ausdehnung + RAND;
			setPreferredSize(new Dimension(groesse, groesse));

			// Initialisiere das verwendete JFrame und setze dich selbst
			// (Panel-Objekt) als content pane.
			_frame = new JFrame("ArrayVisualizer");
			_frame.setResizable(false);
			_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			_frame.getContentPane().add(this);
			_frame.setBackground(BACKGROUND_COLOR);
			_frame.pack();
			_frame.setVisible(true);
		}

		/**
		 * Gib die Pixelkoordinate fue die uebergebene Position zurueck.
		 * 
		 * @param position
		 *            Eine Position deren Pixelkoordinate zurueckgegeben werden
		 *            soll.
		 * @return eine Pixelkoordinate fue die uebergebene Position zurueck.
		 */
		private int screenAt(int position) {
			return RAND + position * _punktGroesse;
		}

		/**
		 * Zeichnet einen Punkt mit der entsprechenden Punktgroesse auf den
		 * Bildschirm.
		 * 
		 * @param x
		 *            x-Koordinate des Punkts
		 * @param y
		 *            y-Koordinate des Punkts
		 * @param color
		 *            Die zu verwendende Farbe
		 * @param g
		 *            Ein zu verwendendes Graphics-Objekt auf dem gezeichnet
		 *            werden soll
		 */
		private void zeichnePunkt(int x, int y, Color color, Graphics g) {
			y = _intArray.length - 1 - y;
			g.setColor(color);
			g.fillRect(screenAt(x), screenAt(y), _punktGroesse, _punktGroesse);
		}

		/**
		 * Zeichnet die angegebene Position.
		 * 
		 * @param x
		 *            Eine Position
		 */
		public void zeichnePosition(int x) {
			repaint(screenAt(x), RAND, _punktGroesse, _ausdehnung);
		}

		/**
		 * Zeichnet das Panel.
		 * 
		 * @param g
		 *            Ein Graphics-Objekt auf dem gezeichnet werden soll.
		 */
		public void paint(Graphics g) {
			super.paint(g);
			for (int i = 0; i < _intArray.length; ++i) {
				Color color = POINT_COLOR;
				if (i == _selected) {
					color = SELECTED_POINT_COLOR;
				}

				zeichnePunkt(i, _intArray[i], color, g);
			}
		}

		/**
		 * Selektiert eine Position, so dass diese hervorgehoben gezeichnet
		 * wird.
		 * 
		 * @param position
		 *            Eine Position, die selektiert werden soll.
		 */
		public void selectPoint(int position) {
			_selected = position;
		}

		/**
		 * Deselktiert die augenblicklich selektierte Position.
		 */
		public void deselectPoint() {
			_selected = -1;
		}

		private static final long serialVersionUID = -1621544717859097232L;
	}
}
