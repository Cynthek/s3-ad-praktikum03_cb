package bubblesort;

import static org.junit.Assert.*;

import org.junit.Test;

public class SortiererTest
{

    @Test
    public void testTauschen()
    {
	// Zwei falsch geordnete Werte werden getauscht.
	int[] testArray = new int[2];
	testArray[0] = 5;
	testArray[1] = 2;
	Sortierer.sort(testArray);
	assertEquals(2, testArray[0]);
	assertEquals(5, testArray[1]);

	// Zwei richtig geordnete Werte werden so gelassen.
	int[] test = new int[2];
	test[0] = 2;
	test[1] = 4;
	Sortierer.sort(test);
	assertEquals(2, test[0]);
	assertEquals(4, test[1]);
    }

    @Test
    public void testSortieren()
    {
	int[] test = new int[3];
	test[0] = 4;
	test[1] = 7;
	test[2] = 2;
	Sortierer.sort(test);
	assertEquals(2, test[0]);
	assertEquals(4, test[1]);
	assertEquals(7, test[2]);
    }

    @Test
    public void testSortiereViel()
    {
	int[] test = new int[15];
	// füllt array mit Werten von 0-14 in absteigender Reihenfolge.
	for (int i = 0; i < 15; i++)
	{
	    test[i] = 14 - i;
	}
	Sortierer.sort(test);
	assertEquals(0, test[0]);
	assertEquals(5, test[5]);
	assertEquals(10, test[10]);
	assertEquals(12, test[12]);

    }

    @Test
    public void testSortierenNull()
    {
	int[] test = new int[0];
	Sortierer.sort(test);
    }

    @Test
    public void testSortierenEins()
    {
	int[] test = new int[1];
	test[0] = 1;
	Sortierer.sort(test);
	assertEquals(1, test[0]);
    }

}
